package test.qa.calender.util;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class WaitUtil {
	public final int explicitWaitDefault =
			PropertyUtils.getIntegerProperty("explicitWait", 10);
	
	public void staticWait(final long millis) {
		try {
			TimeUnit.MILLISECONDS.sleep(millis);
		}catch (final InterruptedException e) {
			
		}
	}
	
	public void waitForElementToBeClickable(final WebElement
			element, final WebDriver driver) {
		new WebDriverWait(driver, this.explicitWaitDefault)
		.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void waitForElementToBeInvisible(final By locator,
			final WebDriver driver) {
		long s = System.currentTimeMillis();
		new WebDriverWait(driver, this.explicitWaitDefault)
		.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}


}
