package test.qa.calender.pages;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;



public class CalenderPO extends Driver {
	PageObjects calendarPage;
	
	
	public CalenderPO() {
		super();
		calendarPage = new PageObjects();
		PageFactory.initElements(driver, calendarPage);
	}
	
	public boolean validateAddEventPageIsDisplayed() {
		if(calendarPage.title.isDisplayed()) {
			return true;
		}
		
		return false;
	}
	
	public void setReccuringMeeting() {
		
		//Select start date criteria
		calendarPage.start.click();
		calendarPage.startDate.click();
		calendarPage.selectTime.click();
		calendarPage.timeHours.sendKeys("2");
		calendarPage.timeMin.sendKeys("00");
		calendarPage.timeAMPM.sendKeys("PM");
		
		//selectStartDate
		
		calendarPage.end.click();
		calendarPage.endDate.click();
		calendarPage.selectTime.click();
		calendarPage.timeHours.sendKeys("3");
		calendarPage.timeMin.sendKeys("00");
		calendarPage.timeAMPM.sendKeys("PM");
		
		
		//click on Done button
		
		calendarPage.doneButton.click();
		
		
		//Verify Start and End date is selected [TODO] [By read and compare]
		
		//Select Recurring every 1 day
		//Click on don't repeat
		calendarPage.dontRepeat.click();
		
		//Verify Repeat page is displayed [TODO]
		//Click on repeat on repeat every 1 day
		calendarPage.recurring.click();

		//Click on save
		
		

	}
	
	public boolean validatecalendarPage() {
		if(calendarPage.addEventButton.isDisplayed()){
			
			return true;
		}
		return false;
	}
	

	
	class PageObjects{
		DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		
		@AndroidFindBy(id = "com.samsung.android.calendar:id/floating_action_button]")
		AndroidElement addEventButton;
		
		@AndroidFindBy(id = "com.samsung.android.calendar:id/title]")
		AndroidElement title;
		
		@AndroidFindBy(xpath = "android.widget.TextView[conatains(@text,'20'])")
		AndroidElement startDate;
		
		@AndroidFindBy(xpath = "android.widget.TextView[conatains(@text,'30'])")
		AndroidElement endDate;
		
		@AndroidFindBy(xpath = "//android.widget.LinearLayout//android.widget.TextView[@text ='Start']")
		AndroidElement start;
		
		@AndroidFindBy(xpath = "//android.widget.LinearLayout//android.widget.TextView[@text ='End']")
		AndroidElement end;
		
		@AndroidFindBy(xpath = "//android.widget.NumberPicker[1]")
		AndroidElement selectTime;
		
		@AndroidFindBy(xpath = "//android.widget.EditText[1]")
		AndroidElement timeHours;
		
		@AndroidFindBy(xpath = "//android.widget.EditText[2]")
		AndroidElement timeMin;
		
		@AndroidFindBy(xpath = "//android.widget.EditText[3]")
		AndroidElement timeAMPM;
		
		@AndroidFindBy(id = "com.samsung.android.calendar:id/button1")
		AndroidElement doneButton;
		
		@AndroidFindBy(id = "com.samsung.android.calendar:id/repeat_text")
		AndroidElement dontRepeat;
		

		@AndroidFindBy(id = "com.samsung.android.calendar:id/repeat_checkbox_set_repeat")
		AndroidElement recurring;
		
		
			
	}
	
}
